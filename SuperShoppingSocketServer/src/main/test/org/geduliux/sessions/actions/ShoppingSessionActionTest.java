package org.geduliux.sessions.actions;

import java.io.IOException;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.testutils.ActionTestUtils;
import org.geduliux.websockets.ShoppingSessionWebsocket;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ShoppingSessionActionTest {
	
	private String sessionID;
	private Class<ShoppingSessionAction> classToTest = ShoppingSessionAction.class;
	
	@Before
	public void init(){
		sessionID = ActionTestUtils.makeSession();
		ShoppingSessionWebsocket.getShoppingsessionmap().put(sessionID, ActionTestUtils.mockWebsocketSessionsSet());
	}
	
	@Test
	public void testSendToAllSessions() throws IOException {
		ShoppingSessionAction sessionAction = Mockito.mock(classToTest, Mockito.CALLS_REAL_METHODS);
		sessionAction.sendToAllSessions(sessionID, ActionTestUtils.SOMEACTION, ActionTestUtils.SOMEDATA);
		for(Session session : ShoppingSessionWebsocket.getShoppingsessionmap().get(sessionID)){
			Mockito.verify(session,Mockito.times(1)).isOpen();
			Mockito.verify(session,Mockito.times(1)).getRemote();
			Mockito.verify(
					session.getRemote(),
					Mockito.times(ShoppingSessionWebsocket
							.getShoppingsessionmap().get(sessionID).size()))
					.sendString(
							sessionAction.new DataPack().JSON(
									ActionTestUtils.SOMEACTION,
									ActionTestUtils.SOMEDATA));
		}
	}
	
	@Test
	public void testSendToSingleSession() throws IOException{
		ShoppingSessionAction sessionAction = Mockito.mock(classToTest, Mockito.CALLS_REAL_METHODS);
		Session session = ShoppingSessionWebsocket.getShoppingsessionmap().get(sessionID).iterator().next();
		sessionAction.sendToSingleSession(session, ActionTestUtils.SOMEACTION, ActionTestUtils.SOMEDATA);
		Mockito.verify(session,Mockito.times(1)).isOpen();
		Mockito.verify(session,Mockito.times(1)).getRemote();
		Mockito.verify(session.getRemote(), Mockito.times(1)).sendString(
				sessionAction.new DataPack().JSON(ActionTestUtils.SOMEACTION,
						ActionTestUtils.SOMEDATA));
	}
	
}
