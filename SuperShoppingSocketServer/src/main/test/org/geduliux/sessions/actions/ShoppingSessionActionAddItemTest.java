package org.geduliux.sessions.actions;

import org.geduliux.sessions.ShoppingSession;
import org.geduliux.sessions.testutils.ActionTestUtils;
import org.geduliux.shoppinglist.ShoppingListItem;
import org.geduliux.websockets.ShoppingSessionWebsocket;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.google.gson.Gson;

public class ShoppingSessionActionAddItemTest {
	
	@Mock
	private ShoppingSession shoppingSessionMock;
	
	private String sessionID;
	
	@Before
	public void init(){
		sessionID = ActionTestUtils.makeSession();
		ShoppingSessionWebsocket.getShoppingsessionmap().put(sessionID, ActionTestUtils.mockWebsocketSessionsSet());
	}
	
	@Test
	public void testDoAction() {
		ShoppingSessionAction addItemAction = new ShoppingSessionActionAddItem();
		//ShoppingSession ShoppingSessionMock = Mockito.mock(ShoppingSession.class, Mockito.CALLS_REAL_METHODS);
		addItemAction.doAction(sessionID, ActionTestUtils.mockShoppingListItemJSON(), ActionTestUtils.SOMEACTION, null);
		//Mockito.when(ShoppingSession.addItem(sessionID, new Gson().fromJson(ActionTestUtils.mockShoppingListItemJSON(),ShoppingListItem.class))).thenReturn(true);
		//PowerMockito.verifyStatic();
		Mockito.verify(shoppingSessionMock,Mockito.times(1)).addItem(sessionID, new Gson().fromJson(ActionTestUtils.mockShoppingListItemJSON(),ShoppingListItem.class));
		//Mockito.verify(ShoppingSession.addItem(sessionID, new Gson().fromJson(ActionTestUtils.mockShoppingListItemJSON(),ShoppingListItem.class))).
	}


}
