package org.geduliux.sessions.testutils;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;
import org.geduliux.sessions.ShoppingSessionCache;
import org.geduliux.shoppinglist.ShoppingList;
import org.geduliux.shoppinglist.ShoppingListItem;
import org.geduliux.shoppinglist.ShoppingListStatus;
import org.mockito.Mockito;

import com.google.gson.Gson;

public class ActionTestUtils {
	
	public static final String SOMEACTION = "Action";
	public static final String SOMEDATA = "data";
	private static Map<String,ShoppingList> fakeCache = Collections.synchronizedMap(new HashMap<String, ShoppingList>());
	
	public static Set<Session> mockWebsocketSessionsSet() {
		Set<Session> websocketSessionSet = new HashSet<Session>();
		Session session1 = Mockito.mock(Session.class);
		Session session2 = Mockito.mock(Session.class);
		RemoteEndpoint rep = Mockito.mock(RemoteEndpoint.class);
		Mockito.when(session1.getRemote()).thenReturn(rep);
		Mockito.when(session2.getRemote()).thenReturn(rep);
		Mockito.when(session1.isOpen()).thenReturn(true);
		Mockito.when(session2.isOpen()).thenReturn(true);
		websocketSessionSet.add(session1);
		websocketSessionSet.add(session2);
		return websocketSessionSet;
	}
	
	public static void mockSessionCache(){		
		Mockito.when(ShoppingSessionCache.getSessionCache()).thenReturn(fakeCache);
	}
	
	public static String makeSession(){
		return ShoppingSession.startNewSession();
	}
	
	public static String mockShoppingListItemJSON(){
		ShoppingListItem shoppingListItem = new ShoppingListItem();
		shoppingListItem.setShoppingListItemDescription("Some cucumber");
		shoppingListItem.setShoppingListStatus(ShoppingListStatus.NEEDED);
		return new Gson().toJson(shoppingListItem);
	}
}
