var ShoppingSessionDataPack;
var sessionid;
var NODATA = " ";
var websocket;

function init() {
	sessionid = getUrlVars()["id"];
	websocket = new WebSocket("ws://localhost:8081/session");

	websocket.onopen = function() {
		//document.getElementById("output").innerHTML += "<p style='color: red;'>> JOINED </p>";
		sendMessage("JOIN", sessionid);
		sendMessage("GETLIST", sessionid);
	};

	websocket.onmessage = function(evt) {
		ShoppingSessionDataPack = JSON.parse(evt.data);
//		document.getElementById("output").innerHTML += "<p style='color: red;'>> received: "
//				+ evt.data + " </p>";
		actionToDo(ShoppingSessionDataPack);
	};

	websocket.onerror = function(evt) {
		document.getElementById("output").innerHTML += "<p style='color: red;'>> ERROR: "
				+ evt.data + "</p>";
	};
	return websocket;
};

function requestQR() {
	sendMessage("QRCODE", NODATA);
};

function requestADD(description) {
	var item = {
		shoppingListItemDescription : "",
		shoppingListStatus : ""
	};
	// alert(description);
	item.shoppingListItemDescription = description;
	// alert("2");
	sendMessage("ADD", JSON.stringify(item));
	// alert("3");
}

function requestDELETE(description) {
	var item = {
		shoppingListItemDescription : "",
		shoppingListStatus : ""
	};
	// alert(description);
	item.shoppingListItemDescription = description;
	// alert("2");
	sendMessage("DELETE", JSON.stringify(item));
}

function requestCHS(description) {
	var item = {
		shoppingListItemDescription : "",
		shoppingListStatus : ""
	};
	// alert(description);
	item.shoppingListItemDescription = description;
	// alert("2");
	sendMessage("CHANGESTATUS", JSON.stringify(item));
}

function sendMessage(action, data) {
//	document.getElementById("output").innerHTML += "<p>> SENT: " + action + ","
//			+ sessionid + "," + data + "</p>";
	websocket.send(action + "," + sessionid + "," + data);
};

function actionToDo(DataPack) {
	// alert(DataPack.action)
	switch (DataPack.action) {
	case "QRCODE":
		displayQR(DataPack.data);
		break;
	case "JOIN":
		document.getElementById("output").innerHTML += "<p> Connected </p>";
		break;
	case "GETLIST":
		fillList(DataPack.data);
		break;
	case "ADD":
		addItem(DataPack.data);
		break;
	case "DELETE":
		deleteItem(DataPack.data);
		break;
	case "CHANGESTATUS":
		changeStatus(DataPack.data);
		break;
	}
};

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
			function(m, key, value) {
				vars[key] = value;
			});
	return vars;
}

function fillList(data) {
	for (i = 0; i < data.length; i++) {
		item = data[i];
		addItem(data[i]);
	};
};

function deleteItem(data){
	document.getElementById(data.shoppingListItemDescription).remove();
}

function changeStatus(data){
	var text = document.createElement("font");// Create a <button> element
	text.appendChild(document.createTextNode(data.shoppingListItemDescription));
	text.setAttribute("size","3");
	text.setAttribute("value", data.shoppingListItemDescription );
	text.setAttribute("color", "green" );
	text.setAttribute("id", data.shoppingListItemDescription );
//	document.getElementById("output").appendChild(text);
//	document.getElementById("output").appendChild(document.createElement("br"));
	var element = document.getElementById(data.shoppingListItemDescription);
	var parent = element.parentNode;
	parent.replaceChild(text,element);
}

function addItem(data) {
	if(data.shoppingListStatus == "NEEDED"){
		var btn = document.createElement("input");// Create a <button> element
		btn.setAttribute("type","button");
		btn.setAttribute("value", data.shoppingListItemDescription );
		btn.setAttribute("id", data.shoppingListItemDescription );
		btn.onclick= function(){requestCHS(data.shoppingListItemDescription);};
		document.getElementById("output").appendChild(btn);
		document.getElementById("output").appendChild(document.createElement("br"));
	} else {
		var text = document.createElement("font");// Create a <button> element
		text.appendChild(document.createTextNode(data.shoppingListItemDescription));
		text.setAttribute("size","3");
		text.setAttribute("value", data.shoppingListItemDescription );
		text.setAttribute("color", "green" );
		text.setAttribute("id", data.shoppingListItemDescription );
		document.getElementById("output").appendChild(text);
		document.getElementById("output").appendChild(document.createElement("br"));
	}
};

function displayQR(data) {
	document.getElementById("qrcode").src = "data:image/png;base64," + data;
};
// function
// sendMessage(this.value); this.value = null;
window.addEventListener("load", init, false);