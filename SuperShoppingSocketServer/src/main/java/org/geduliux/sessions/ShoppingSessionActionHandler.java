package org.geduliux.sessions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.actions.ShoppingSessionAction;
import org.geduliux.sessions.actions.ShoppingSessionActionAddItem;
import org.geduliux.sessions.actions.ShoppingSessionActionChangeStatus;
import org.geduliux.sessions.actions.ShoppingSessionActionDeleteItem;
import org.geduliux.sessions.actions.ShoppingSessionActionGetQR;
import org.geduliux.sessions.actions.ShoppingSessionActionGetShoppingList;
import org.geduliux.sessions.actions.ShoppingSessionActionJoinSession;

public enum ShoppingSessionActionHandler {
	//just add whatever acctions you've thinked about and implement it
	JOIN(new ShoppingSessionActionJoinSession()),
	QRCODE(new ShoppingSessionActionGetQR()),
	ADD(new ShoppingSessionActionAddItem()),
	GETLIST(new ShoppingSessionActionGetShoppingList()),
	DELETE(new ShoppingSessionActionDeleteItem()),
	CHANGESTATUS(new ShoppingSessionActionChangeStatus());
	
	String shoppingSessionID;
	ShoppingSessionAction shoppingSessionAction;
	Session websocketSession;
	String nameToOverride;
	
	private ShoppingSessionActionHandler(ShoppingSessionAction shoppingSessionAction){
		this.shoppingSessionAction = shoppingSessionAction;
	}
	//When you know that your action response would be other than it requested you indicate with a enum name

	//did this because if there's other implementations or new websocket that will not need some of the data 
	public ShoppingSessionActionHandler withSessionID(String ID){
		shoppingSessionID = ID;
		return this;	
	}
	
	public ShoppingSessionActionHandler andWithSocketSession(Session session){
		websocketSession = session;
		return this;	
	}
	
	public void process(String data){
		shoppingSessionAction.doAction(shoppingSessionID, data, name(), websocketSession);		
	}
	
}
