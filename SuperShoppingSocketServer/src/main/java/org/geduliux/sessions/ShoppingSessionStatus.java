package org.geduliux.sessions;

public enum ShoppingSessionStatus {
	OK(),
	ERR();
	
	String message;

	public String getMessage() {
		return message;
	}

	public ShoppingSessionStatus setMessage(String message) {
		this.message = message;
		return this;
	}	
}
