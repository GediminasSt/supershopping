package org.geduliux.sessions.actions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;
import org.geduliux.shoppinglist.ShoppingListItem;
import org.geduliux.shoppinglist.ShoppingListStatus;

import com.google.gson.Gson;

public class ShoppingSessionActionAddItem extends ShoppingSessionAction{

	@Override
	public void doAction(String SessionID, String Data, String actionName, Session socketSession) {
		ShoppingListItem item = new Gson().fromJson(Data, ShoppingListItem.class);
		item.setShoppingListStatus(ShoppingListStatus.NEEDED);
		if(ShoppingSession.addItem(SessionID, item)){
			sendToAllSessions(SessionID, actionName , item);
			System.out.println("Item "+ item.getShoppingListItemDescription() + " added to:" + SessionID);
		}else{
			System.out.println("Item "+ item.getShoppingListItemDescription() + " FAILED TO ADD:" + SessionID);
		}
	}

}
