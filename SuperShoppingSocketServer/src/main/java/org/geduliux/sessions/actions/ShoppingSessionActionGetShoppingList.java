package org.geduliux.sessions.actions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;

public class ShoppingSessionActionGetShoppingList extends
		ShoppingSessionAction {

	@Override
	public void doAction(String SessionID, String Data,String actionName, Session socketSession){
		sendToSingleSession(socketSession, actionName, ShoppingSession.getList(SessionID));
		System.out.println("ShoppingList sent to:" + SessionID);
	}

}
