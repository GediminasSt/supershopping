package org.geduliux.sessions.actions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;

public class ShoppingSessionActionGetQR extends ShoppingSessionAction {

	@Override
	public void doAction(String SessionID, String Data, String actionName, Session socketSession){
		sendToSingleSession(socketSession, actionName , ShoppingSession.getQRCode(SessionID));
		System.out.println("Sent QR to session:" + SessionID);
	}

}
