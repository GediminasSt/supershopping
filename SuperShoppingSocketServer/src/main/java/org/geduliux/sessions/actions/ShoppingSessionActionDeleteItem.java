package org.geduliux.sessions.actions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;
import org.geduliux.shoppinglist.ShoppingListItem;

import com.google.gson.Gson;


public class ShoppingSessionActionDeleteItem extends ShoppingSessionAction{

	@Override
	public void doAction(String SessionID, String Data, String actionName, Session socketSession) {
		if(ShoppingSession.removeItem(SessionID, new Gson().fromJson(Data, ShoppingListItem.class))){
			System.out.println("Item successfully removed:" + SessionID);
			//Sending that every session needs to update
			sendToAllSessions(SessionID, actionName, Data);
		}else{
			System.out.println("Item removed unsuccessfully:" + SessionID);
		}
	}

}
