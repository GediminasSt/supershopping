package org.geduliux.sessions.actions;

import java.io.IOException;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.websockets.ShoppingSessionWebsocket;

import com.google.gson.Gson;

/**
 * This classes extending this will save alot of data between server and client, because we wont send in whole objects,
 * but the data of interest.
 * @author Gediminas Staponas
 *
 */
public abstract class ShoppingSessionAction {
	
	protected static final String OK = "OK";
	protected static final String NODATA = "";
	
	public abstract void doAction(String sessionID, String data, String actionName, Session socketSession);
	
	//function to data up all of the sessions connected and under shopping session ID
	protected void sendToAllSessions(String SessionID, String action, Object data) {
		for(Session webSession : ShoppingSessionWebsocket.getShoppingsessionmap().get(SessionID)){
			sendToSingleSession(webSession, action, data);
		}
		System.out.println("Sent to all sessions! " + action);
	}
	
	protected void sendToSingleSession(Session session, String action, Object data){		
		try {
			if(session.isOpen()){
				session.getRemote().sendString(new DataPack().JSON(action, data));
			}else{
				//let gc collect closed session on client side
				session = null;
			}
		} catch (IOException e) {
			
		}
	}
	
	protected void actionSuccess(String action, String SessionID){
		
	}
	//data package which will get JSONED and sent to client
	class DataPack{
		private String action;
		private Object data;
		
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
		
		public String JSON(String action, Object data){
			this.action = action;
			this.data = data;
			return new Gson().toJson(this);
		}
	}
}
