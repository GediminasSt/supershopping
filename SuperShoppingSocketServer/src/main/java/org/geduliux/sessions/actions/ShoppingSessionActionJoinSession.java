package org.geduliux.sessions.actions;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.websockets.ShoppingSessionWebsocket;

public class ShoppingSessionActionJoinSession extends ShoppingSessionAction {

	@Override
	public void doAction(String SessionID, String Data, String actionName, Session socketSession){		
		if(!ShoppingSessionWebsocket.getShoppingsessionmap().containsKey(SessionID)){ 
			Set<Session> SessionSet = new HashSet<Session>();
			SessionSet.add(socketSession);
			ShoppingSessionWebsocket.getShoppingsessionmap().put(Data, SessionSet);
		}else{
			ShoppingSessionWebsocket.getShoppingsessionmap().get(Data).add(socketSession);
		}		
		sendToSingleSession(socketSession, actionName, NODATA);
		System.out.println("User joined to session:" + SessionID);
	}

}
