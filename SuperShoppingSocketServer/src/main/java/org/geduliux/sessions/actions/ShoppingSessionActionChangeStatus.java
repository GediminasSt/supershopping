package org.geduliux.sessions.actions;

import org.eclipse.jetty.websocket.api.Session;
import org.geduliux.sessions.ShoppingSession;
import org.geduliux.shoppinglist.ShoppingListItem;

import com.google.gson.Gson;

public class ShoppingSessionActionChangeStatus extends ShoppingSessionAction {

	@Override
	public void doAction(String sessionID, String data, String actionName,
			Session socketSession) {			
		ShoppingListItem item = new Gson().fromJson(data, ShoppingListItem.class);
		if(ShoppingSession.changeStatus(sessionID, item)){
			sendToAllSessions(sessionID, actionName , item);
			System.out.println("Change Status of " + item.getShoppingListItemDescription() +" changed to " + item.getShoppingListStatus().name());
		}else{
			System.out.println("Change Status of " + item.getShoppingListItemDescription() +" FAILED");
		}
	}

}
