package org.geduliux.sessions;

import java.util.List;
import java.util.UUID;

import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

import org.geduliux.shoppinglist.ShoppingList;
import org.geduliux.shoppinglist.ShoppingListItem;
import org.geduliux.shoppinglist.ShoppingListStatus;

import sun.misc.BASE64Encoder;

public class ShoppingSession{
	
	public static String startNewSession(){		
		String uuid = UUID.randomUUID().toString();		
		ShoppingList SessionshoppingList = new ShoppingList();
		SessionshoppingList.setSessionID(uuid);
		SessionshoppingList.setQrCode(new BASE64Encoder().encode(QRCode.from(uuid)
				.to(ImageType.PNG).withSize(300, 300).stream().toByteArray()));
		ShoppingSessionCache.getSessionCache().put(uuid, SessionshoppingList);
		return getSession(uuid).getSessionID();
	}
	
	/**
	 * 
	 * @param SessionID
	 * @param ShoppingListItem
	 * @return
	 */
	public static boolean removeItem(String sesID, ShoppingListItem item){
		List<ShoppingListItem> list = getSession(sesID).getShoppingList();		
		return list.contains(item) ? list.remove(item) : true;
	}
	
	/**
	 * 
	 * @param SessionID
	 * @param ShoppingListItem
	 * @return
	 */
	
	public static boolean addItem(String sesID, ShoppingListItem item){
		return removeItem(sesID, item) && getSession(sesID).getShoppingList().add(item);
	}
	
	/**
	 * 
	 * @param sesID SessionID
	 * @return
	 */
	public static String getQRCode(String sesID){
		return getSession(sesID).getQrCode();
	}
	
	/**
	 * 
	 * @param SessionID
	 * @param ShoppingListItem
	 */
	public static boolean changeStatus(String sesID, ShoppingListItem item){
		if (null != item.getShoppingListStatus()) {
			switch (item.getShoppingListStatus()) {
			case NEEDED:
				item.setShoppingListStatus(ShoppingListStatus.TAKEN);
				break;
			case TAKEN:
				item.setShoppingListStatus(ShoppingListStatus.NEEDED);
				break;
			default:
				break;
			}
		} else {
			//dirty workarround of null status
			item.setShoppingListStatus(ShoppingListStatus.TAKEN);
		}
		return addItem(sesID,item);
	}
	
	/**
	 * 
	 * @param sesID SessionID
	 * @return
	 */
	
	public static List<ShoppingListItem> getList(String sesID){
		return getSession(sesID).getShoppingList();
	}
	
	private static ShoppingList getSession(String ID){
		return ShoppingSessionCache.getSessionCache().get(ID);
	}
	
}
