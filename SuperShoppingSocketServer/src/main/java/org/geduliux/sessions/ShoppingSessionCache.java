package org.geduliux.sessions;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.geduliux.shoppinglist.ShoppingList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * 
 * Daddy Gediminas Staponas
 *
 *
 */
/**
 * Special map. When object is put onto map.,
 */
public class ShoppingSessionCache {
	private final static Map<String,ShoppingList> SESSION_CACHE = Collections.synchronizedMap(new HashMap<String, ShoppingList>(){
		
		private static final long serialVersionUID = 1L;
		
		private final Map<String,ThreadManager> timerMap = new HashMap<String, ThreadManager>();
		private int TIMER_VALUE = 10;
		private final SessionFactory sessionFactory = buildSessionFactory();
		private final Object lock = new Object();
		private SessionFactory buildSessionFactory() {
			try {
				Configuration configuration = new Configuration();
			    configuration.configure();
			    final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
			            configuration.getProperties()).build();
			    return configuration.buildSessionFactory(serviceRegistry);
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed."
						+ ex);
				throw new ExceptionInInitializerError(ex);
			}
		}
		
		public ShoppingList remove(Object key) {
			putObjectToDatabase(super.get(key));
			if(null != timerMap.get(key)){
				timerMap.get(key).getThread().interrupt();
				timerMap.remove(key);
			}
			super.put((String) key, null);
			return super.remove(key);
		};
		
		public ShoppingList get(Object key) {
			if(null == super.get(key)){
				Session session = getHibSession();
				session.beginTransaction();
				Object returnedObj = session.get(ShoppingList.class, (Serializable) key);
				this.put((String) key, (ShoppingList) returnedObj);
				session.getTransaction().commit();
				session.close();
			}else if(null != super.get(key)){
				//refreshing timer
				timerMap.get(key).setTimer(TIMER_VALUE);
			}		
			return super.get(key);
		};
		
		//Puting ShoppingList to superclasses map, then executing timer with ID for the time 
		//Creating new thread which will expire cache according to TIMER_VALUE and upload stuff to DB
		public ShoppingList put(String key, ShoppingList value) {
			super.put(key, value);
			timerMap.put(key, new ThreadManager(key, TIMER_VALUE));
			timerMap.get(key).startCount();
			return super.get(key);
		};
		
		private void putObjectToDatabase(Object object){
				Session session = getHibSession();
				session.beginTransaction();
				session.saveOrUpdate(object);
				session.getTransaction().commit();
				session.close();
		};
		
		private Session getHibSession(){
			return sessionFactory.openSession();
		}
		
		class ThreadManager implements Runnable{
			private String key;
			private int timer;
			private Thread thread;
			
			ThreadManager(String key, int timer){
				this.key = key;
				this.setTimer(timer);
			}
			
			@Override
			public void run() {
				try {
					//avoiding concurrent modification
					int threadtimer = timer;
					synchronized (lock) {
						timer = 0;					
					}
					Thread.sleep(threadtimer*1000);
					synchronized (lock) {
						if(timer == 0){
							SESSION_CACHE.remove(key);
							return;
						}
					}
					run();
				} catch (InterruptedException e) {
					System.out.println("Thread interrupted for removal");;
				}			
			}

			public void setTimer(int timer) {
				this.timer = timer;
			}
			
			public void startCount(){		
				thread = new Thread(this,key);
				thread.start();
			}

			public Thread getThread() {
				return thread;
			}		
		}
		
	});
	
	public static Map<String, ShoppingList> getSessionCache() {
		return SESSION_CACHE;
	}
}
