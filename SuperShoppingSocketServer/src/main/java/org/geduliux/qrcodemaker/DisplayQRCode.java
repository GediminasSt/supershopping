package org.geduliux.qrcodemaker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DisplayQRCode {
	public static void display(OutputStream outputStream,File qrCode){
		try {
			doWriting(outputStream, qrCode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void doWriting(OutputStream outputStream, File file) throws IOException{
		InputStream inputStream = new FileInputStream(file);
		int read = 0;
		final byte[] bytes = new byte[1024];
		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		inputStream.close();
	}
	
}
