package org.geduliux.shoppinglist;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
@Embeddable
public class ShoppingListItem implements Serializable{
		
	private static final long serialVersionUID = 1L;
	@Column
	public String shoppingListItemDescription;
	@Enumerated(EnumType.STRING)
	@Column
	public ShoppingListStatus shoppingListStatus;
	//will implement it later if needed
	
	public String getShoppingListItemDescription() {
		return shoppingListItemDescription;
	}
	public void setShoppingListItemDescription(String shoppingListItemDescription) {
		this.shoppingListItemDescription = shoppingListItemDescription;
	}
	
	public ShoppingListStatus getShoppingListStatus() {
		return shoppingListStatus;
	}
	public void setShoppingListStatus(ShoppingListStatus shoppingListStatus) {
		this.shoppingListStatus = shoppingListStatus;
	}
	
	@Override
	public boolean equals(Object obj) {
		//just comparitor for comparing 2 hash sets wich are implemented down
		if(obj == this){
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		ShoppingListItem compared = (ShoppingListItem) obj;
		return this.hashCode() == compared.hashCode();
	}
	
	@Override
	public int hashCode() {
		//just making sure it's not all over the place making hash set conciderable from shoppingListItemDescrptor not anything else is needed
		//i think
		final int prime = 31;
		return prime + (
				(shoppingListItemDescription != null || shoppingListItemDescription.trim().length() > 0) ? 
						shoppingListItemDescription.hashCode() : 0);
	}
	
}
