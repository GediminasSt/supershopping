package org.geduliux.shoppinglist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;

@Entity
public class ShoppingList implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "SESSIONID",nullable = false)
	public String sessionID;
	@Lob
	@Column(length=1024)
	public String qrCode;
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(joinColumns=@JoinColumn(name="BELONGS_TO_SESSION"))
	public List<ShoppingListItem> shoppingList = new ArrayList<ShoppingListItem>();
	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public List<ShoppingListItem> getShoppingList() {
		return shoppingList;
	}
	public void setShoppingList(List<ShoppingListItem> shoppingList) {
		this.shoppingList = shoppingList;
	}
	

}
