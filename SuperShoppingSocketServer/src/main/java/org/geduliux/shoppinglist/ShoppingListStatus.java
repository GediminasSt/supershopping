package org.geduliux.shoppinglist;


public enum ShoppingListStatus {
	NEEDED,
	TAKEN
}
