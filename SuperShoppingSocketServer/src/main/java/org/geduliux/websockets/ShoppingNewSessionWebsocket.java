package org.geduliux.websockets;

import java.io.IOException;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.geduliux.sessions.ShoppingSession;

@WebSocket
public class ShoppingNewSessionWebsocket {
	
	@OnWebSocketConnect
	public void onOpen(Session session){		
		System.out.println("client Connected");

		try {
			String uuid = ShoppingSession.startNewSession();
			//String shit = new Gson().toJson(ShoppingSessionCache.getSessionCache().get(uuid));
			session.getRemote().sendString(uuid);
			//Thread.sleep(3000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@OnWebSocketMessage
	public void onMessage(Session session, String msg){
		
	}
	
	@OnWebSocketClose
	public void onClose(Session session, int x, String text){
		System.out.println("closed");
	}
	
	@OnWebSocketError
    public void onError(Throwable error) {
		System.out.println("errrrr");
	}
}
