package org.geduliux.websockets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.geduliux.sessions.ShoppingSessionActionHandler;

@WebSocket
public class ShoppingSessionWebsocket {
	
	public final static int INDEX_OF_ACTION = 0;
	public final static int INDEX_OF_SESSIONID = 1;
	public final static int INDEX_OF_DATA = 2;
	
	public static final Map<String,Set<Session>> ShoppingSessionPool = new HashMap<String, Set<Session>>();
		
	@OnWebSocketConnect
	public void onOpen(Session session){
	}
	
	public static Map<String,Set<Session>> getShoppingsessionmap() {
		return ShoppingSessionPool;
	}

	@OnWebSocketMessage
	public void onMessage(Session session, String msg) {
		String[] messageParts = msg.split("\\,", 3); // taking 1st 2 sepparations as important ones other ones - data
		
		try{
			ShoppingSessionActionHandler.valueOf(messageParts[INDEX_OF_ACTION])
					.withSessionID(messageParts[INDEX_OF_SESSIONID])
					.andWithSocketSession(session).process(messageParts[INDEX_OF_DATA]);
		}catch (IndexOutOfBoundsException e){
			//TODO: need to handle it much beter
			try {
				session.getRemote().sendString("ERR,Failed to:" + messageParts[INDEX_OF_ACTION]);
			} catch (IOException e1) {
				e1.printStackTrace();
				System.out.println("Couldn't find session");
			}
		}
	}
	
	@OnWebSocketClose
	public void onClose(Session session, int x, String text){
		System.out.println("closed conn");	
	}
	
	@OnWebSocketError
    public void onError(Throwable error) {
		System.out.println("errrrr");
	}
}
