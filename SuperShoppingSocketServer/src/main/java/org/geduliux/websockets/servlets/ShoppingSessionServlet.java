package org.geduliux.websockets.servlets;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.geduliux.websockets.ShoppingSessionWebsocket;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = {"/session"} )
public class ShoppingSessionServlet extends WebSocketServlet{

	@Override
	public void configure(WebSocketServletFactory factory) {
		factory.getPolicy().setIdleTimeout(1000 * 3600);
		factory.register(ShoppingSessionWebsocket.class);		
		System.out.println("ShoppingSessionWebsocket Registered");		
	}

}
