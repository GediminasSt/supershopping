package org.geduliux.websockets.servlets;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.geduliux.websockets.ShoppingNewSessionWebsocket;


@SuppressWarnings("serial")
@WebServlet(urlPatterns = {"/newsession"} )
public class ShoppingNewSessionServlet extends WebSocketServlet{

	@Override
	public void configure(WebSocketServletFactory factory) {
		factory.register(ShoppingNewSessionWebsocket.class);		
		System.out.println("ShoppingNewSessionWebsocket Registered");
	}

}
